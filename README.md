# Plugin Update Products from Zetti ERP

Repositorio público para el plugin privado diu-update-products-api

Plugin para actualización de Stock y Precios de los productos de la API de ZETTI (https://zetti.com.ar/)
hacia los productos de WooCommerce.

Repositorio privado https://gitlab.com/diurvan/diu-update-products-api
diu-update-products-api

Se utiliza los endpoints para la integración.
<br/>
http://demo.zetti.com.ar/api-rest
